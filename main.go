package main

import (
	"context"
	"fmt"
	"github.com/micro/go-micro"
	"log"

	pb "gitlab.com/victorsteven/shippy-service-consignment/proto/consignment"
	vesselProto "gitlab.com/victorsteven/shippy-service-vessel/proto/vessel"

	"sync"
)

const (
	port = ":50051"
)

type repository interface {
	Create(*pb.Consignment) (*pb.Consignment, error)
	GetAll() []*pb.Consignment
}

type Repository struct {
	mu sync.RWMutex
	consignments []*pb.Consignment
}

//Create a new consignment
func (repo *Repository) Create(consignment *pb.Consignment) (*pb.Consignment, error) {
	repo.mu.Lock()
	updated := append(repo.consignments, consignment)
	repo.consignments = updated
	repo.mu.Unlock()
	return consignment, nil
}

func (repo *Repository) GetAll() []*pb.Consignment {
	return repo.consignments
}

type service struct {
	repo repository
	vesselClient vesselProto.VesselServiceClient
}

func (s *service) CreateConsignment(ctx context.Context, req *pb.Consignment, res *pb.Response)  error {

	//here we call a client instance of our vessel service with our consignment weight and the amount of containers as the capacity value
	vesselResponse, err := s.vesselClient.FindAvailable(context.Background(), &vesselProto.Specification{
		Capacity:             int32(len(req.Containers)),
		MaxHeight:            req.Weight,
	})
	if err != nil {
		return err
	}
	log.Printf("Found vessel: %s \n", vesselResponse.Vessel.Name)

	//set the vesselId as the vessel we got back from our vessel service
	req.VesselId = vesselResponse.Vessel.Id

	//save our consigment
	consignment, err := s.repo.Create(req)
	if err != nil {
		return err
	}
	res.Created = true
	res.Consignment = consignment
	return nil
}

func (s *service) GetConsignments(ctx context.Context, req *pb.GetRequest, res *pb.Response) error {
	consignments := s.repo.GetAll()
	res.Consignments = consignments
	return nil
}


func main() {
	repo := &Repository{}

	srv := micro.NewService(
		//name of our package
		micro.Name("shippy.service.consignment"),
	)
	//init will parse the command line flags
	srv.Init()

	vesselClient := vesselProto.NewVesselServiceClient("shippy.service.vessel", srv.Client())

	//register handler
	//pb.RegisterShippingServiceHandler(srv.Server(), &service{repo})
	pb.RegisterShippingServiceHandler(srv.Server(), &service{repo, vesselClient})

	//Run the server
	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}

	//set-up our gRPC server.
	//lis, err := net.Listen("tcp", port)
	//if err != nil {
	//	log.Fatalf("failed to listen: %v", err)
	//}
	//s := grpc.NewServer()
	//pb.RegisterShippingServiceServer(s, &service{repo})
	////Register reflection service on gRPC server
	//reflection.Register(s)
	//
	//log.Println("Running on port: ", port)
	//if err := s.Serve(lis); err != nil {
	//	log.Fatalf("failed to serve: %v", err)
	//}
}
