module gitlab.com/victorsteven/shippy-service-consignment

go 1.13

require (
	github.com/golang/protobuf v1.3.5
	github.com/micro/go-micro v1.18.0
	github.com/micro/protobuf v0.0.0-20180321161605-ebd3be6d4fdb // indirect
	gitlab.com/victorsteven/shippy-service-vessel v0.0.0-20200327005549-f88abf306a9f // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	google.golang.org/grpc v1.26.0
)
